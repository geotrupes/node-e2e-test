import puppeteer from 'puppeteer';
import express from "express";
import test from "tape";

const app = express();

app.use(express.static('./'));
app.listen(3000);


(async () => {
  console.log("Starting tests runner!");

  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true
  });

  const page = await browser.newPage();
  await page.goto('http://localhost:3000', { "waitUntil": "networkidle0" });

  test("Should create rocket list", async t => {
    const rockets_list = await page.evaluate(() => document.querySelector('#rockets-list').children)
    t.equal(Object.entries(rockets_list).length > 0, true, "Rocket list has more than 0 children")
    t.end()
  })

  test("Can click on rockets list item", async t => {
    page.evaluate(() => document.querySelector('#rockets-list').children[1].click());
    page.evaluate(() => document.querySelector('#rockets-list').children[1].classList.contains("chosen"))
      .then((classSelected) => {
        t.equal(classSelected, true, "Selected rocket has class chosen");
        t.end()
      })
  })

  test("Can view rocket launches information", async t => {
    page.evaluate(() => document.querySelector('#search-button').click())
    await page.waitForNavigation({ waitUntil: "networkidle2" })
    await page.waitForSelector("#search-page")
    const launches_list = await page.evaluate(() => document.querySelector('#search-page').children)
    t.equal(Object.entries(launches_list).length > 0, true, "Rocket launches has more than 0 children")
    t.end()
  })

  test("Rocket launches details has correct information", async t => {
    const launch_details = await page.evaluate(() => {
      const first_launch = document.querySelector('#search-page').children[1]
      return Array.from(first_launch.childNodes)
        .filter(x => x.attributes)
        .map(item => item.getAttribute("data-type"))
    })

    t.equal(launch_details[0], "name", "Rocket launch details has name")
    t.equal(launch_details[1], "date", "Rocket launch details has date")
    t.equal(launch_details[2], "long-name", "Rocket launch details has long-name")
    t.equal(launch_details[3], "details", "Rocket launch details has details")

    t.end()
    await browser.close()
    process.exit()
  })
})();