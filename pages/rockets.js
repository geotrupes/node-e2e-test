import { HTML } from "https://unpkg.com/kelbas";
import Element from "../helpers/element.js";

//Shows all the rockets list
export default class Rockets extends Element {

  async install() {
    this.chosen_rocket = null
    this.rockets_data = await this.getRockets();
  }

  chooseRocket(rocket) {
    this.chosen_rocket = rocket.id;
    this.update();
  }

  async getRockets() {
    console.time("rocket-api-request main data");
    const data = await fetch("https://api.spacexdata.com/v2/rockets").then(res => res.json());
    console.timeEnd("rocket-api-request main data");
    return data;
  }

  async search() {
    if(!this.chosen_rocket) {
      window.alert("Please choose a rocket type");
      return
    }
    const date = new Date(this.querySelector("#start-date").value).getFullYear()
    console.info(`searching for ${this.chosen_rocket}, ${date}`)
    this.router.navigate(`/rocket/${this.chosen_rocket}/${date}`)
  }

  async render() {
    return HTML`<div class="page" id="rockets-page">
                        <section id="rockets-list">
                            ${this.rockets_data.map(rocket => {
                                return HTML`<div onclick=${this.chooseRocket.bind(this, rocket)} class="rocket-item ${this.chosen_rocket === rocket.id ? "chosen " : ''}" >
                                                <span class="rocket-name">
                                                    ${rocket.name}
                                                </span>
                                                <span> Active: ${rocket.active ? "true" : "false"}</span>
                                            </div>`
                            })}
                        </section>
                        <section id="date-picker">
                            <label for="start">Start date:</label>
                            <input type="date" id="start-date" name="rocket-start" value="2018-07-22">
                        </section>

                        <div id="search-button" onclick=${this.search.bind(this)}>
                           Search
                        </div>
                    </div>`;
  }
}
