import { HTML } from "https://unpkg.com/kelbas"
import Element from "../helpers/element.js";

//Shows the search results for specific rocket and date
export default class Search extends Element {
   async install() {
        console.time("rocket-api-request search");
        this.launches = await fetch(`https://api.spacexdata.com/v2/launches?launch_year=${this.date}&rocket_id=${this.rocket_id}`)
        .then(res => res.json())
        console.timeEnd("rocket-api-request search");
   }

    render() {
        return HTML`<div class="page" id="search-page">
                        ${!this.launches.length && `<h3>No launches found!</h3>`}
                        ${this.launches.map((item) => {
                            return HTML`<div class="launch-item">
                                            <span data-type="name" class="launch-item-name">Rocket name: ${item.rocket.rocket_name}</span>
                                            <span data-type="date" class="launch-item-date">Launch Date: ${item.launch_date_utc}</span>
                                            <span data-type="long-name" class="launch-item-name">Long Launch Site Name: ${item.launch_site.site_name_long}</span>
                                            <p data-type="details" class="launch-item-details">Details: ${item.details}</p>
                                        </div>`
                        })}
                    </div>`
    }
}