import Router from "https://unpkg.com/navigo@6.0.2/src/index.js";
import { HTML } from "https://unpkg.com/kelbas"
import * as pages from "./pages/index.js";

const router = new Router();

for (let key in pages) {
  pages[key].prototype.router = router;
  customElements.define(`${key}-page`, pages[key]);
}

router.on("/", () => {
  const element = HTML`<rockets-page id="root"></home-page>`;
  document.querySelector("#root").replaceWith(element);
});

router.on("/rocket/:id/:date", params => {
  const element = HTML`<search-page id="root"></search-page>`;
        element.rocket_id = params.id
        element.date = params.date
        
      document.querySelector("#root").replaceWith(element);
});

router.resolve();